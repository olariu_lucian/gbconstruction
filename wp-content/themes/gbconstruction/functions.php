<?php
/**
 * ImpactPro includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$impactpro_includes = [
  'lib/assets.php',             // Scripts and stylesheets
  'lib/extras.php',             // Custom functions
  'lib/setup.php',              // Theme setup
  'lib/theme-options.php',      // Theme options
  'lib/nav.php',                // Bootstrap Nav Walker
  'lib/cpt-services.php',       // Services Custom Post Type
  'lib/cpt-team.php',           // Team Custom Post Type
  'lib/cpt-projects.php',       // Projects Custom Post Type + Taxonomy
  'lib/titles.php',             // Page titles
  'lib/wrapper.php',            // Theme wrapper class
  'lib/customizer.php'          // Theme customizer
];

foreach ($impactpro_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'gbconstruction'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);
