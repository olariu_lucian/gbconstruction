/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var ImpactPro = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
          //baguetteBox.run('.grid');
          //baguetteBox.run('.siteorigin-widget-tinymce');
          baguetteBox.run('.main');
          $(document).ready(function(){
              //$('.hp-reviews .reviews-list').addClass('owl-carousel owl-theme');
              $('.hp-reviews .reviews-list').owlCarousel({
                  items:3,
                  //lazyLoad:true,
                  //loop:false,
                  dots:false,
                  margin:10,
                  nav:true,
                  navText: [
                      "<i class='fa fa-long-arrow-left'></i>",
                      "<i class='fa fa-long-arrow-right'></i>"
                  ],
                      // breakpoint from 0 up
                  responsive : {
                      0 : {
                          items:1
                      },
                      // breakpoint from 480 up
                      480 : {
                          items:1
                      },
                      // breakpoint from 768 up
                      768 : {
                          items:2
                      },
                      1280 : {
                          items:3
                      }
                  }
              });
              $('.hp-projects .home-projects').owlCarousel({
                  items:4,
                  lazyLoad:true,
                  //loop:false,
                  dots:false,
                  margin:10,
                  nav:true,
                  navText: [
                      "<i class='fa fa-long-arrow-left'></i>",
                      "<i class='fa fa-long-arrow-right'></i>"
                  ],
                      // breakpoint from 0 up
                  responsive : {
                      0 : {
                          items:1
                      },
                      // breakpoint from 480 up
                      480 : {
                          items:1
                      },
                      // breakpoint from 768 up
                      768 : {
                          items:2
                      },
                      1280 : {
                          items:3
                      },
                      1600: {
                          items:4
                      }
                  }
              });
          });


      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'post_type_archive_projects': {
      init: function() {
          // Isotope Init -------------------------------------------
          $(window).load(function(){
              var $container = jQuery('.grid').isotope({
                  // options
                  itemSelector: '.grid-item'
              });
              // filter items on button click
              $('.filter-button-group').on( 'click', 'button', function() {
                  var filterValue = jQuery(this).attr('data-filter');
                  $container.isotope({ filter: filterValue });
              });
          });

          // flatten object by concatting values
          function concatValues( obj ) {
              var value = '';
              for ( var prop in obj ) {
                  value += obj[ prop ];
              }
              return value;
          }
          // store filter for each group
          var filters = {};

          $('.filters').on( 'click', '.button', function() {
              var $this = $(this);
              // get group key
              var $buttonGroup = $this.parents('.button-group');
              var filterGroup = $buttonGroup.attr('data-filter-group');
              // set filter for group
              filters[ filterGroup ] = $this.attr('data-filter');
              // combine filters
              var filterValue = concatValues( filters );
              // set filter for Isotope
              $grid.isotope({ filter: filterValue });
          });

          // change is-checked class on buttons
          $('.button-group').each( function( i, buttonGroup ) {
              var $buttonGroup = $( buttonGroup );
              $buttonGroup.on( 'click', 'button', function() {
                  $buttonGroup.find('.is-checked').removeClass('is-checked');
                  $( this ).addClass('is-checked');
              });
          });
          // end isotope
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = ImpactPro;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
