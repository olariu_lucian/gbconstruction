<?php the_content(); ?>
<div class="social-share-container">
    <?php do_action('social_share_icons'); ?>
</div>
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'gbconstruction'), 'after' => '</p></nav>']); ?>
