<?php
use ImpactPro\Titles;
$logo_image = get_field('logo_image','option');
$sn_fb_img = get_field('sn_facebook_img','option');
$sn_pintrest_img = get_field('sn_pintrest_img','option');
global $post;
//echo '<pre>',var_dump($sn_fb_img),'</pre>';
?>
<div class="topbar">
    <div class="container">
        <div class="row clearfix">
            <div class="col-xs-12 text-right">
                <span class="hidden-xs"><?php the_field('social_network_text','option'); ?></span>
                <a href="<?php the_field('sn_facebook_link','option')?>" target="_blank">
                    <img class="img-responsive topbar-social-img" src="<?php echo $sn_fb_img['sizes']['thumbnail'];?>" title="<?php echo $sn_fb_img['title']; ?>" alt="Facebook"/>
                </a>
                <a href="<?php the_field('sn_pinterest_link','option')?>" target="_blank">
                    <img class="img-responsive topbar-social-img" src="<?php echo $sn_pintrest_img['sizes']['thumbnail'];?>" title="<?php echo $sn_pintrest_img['title']; ?>" alt="Pinterest"/>
                </a>
            </div>

        </div>
    </div>
</div>
<div class="topheader">
    <div class="container">
        <div class="row clearfix">
            <div class="logo col-xs-6 col-sm-4 col-sm-push-4">
                <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
                    <img class="img-responsive" src="<?php echo $logo_image['url'];?>" alt="G&B Construction">
                </a>
            </div>
            <div class="header-phone col-xs-6 no-padding-left col-sm-4 col-sm-pull-4">
                <a href="tel:2156598100"><i class="fa fa-phone fa-3x"></i>
                <span class="call-us no-padding"><?php the_field('call_us_text','option');?></span><br/>
                <span class="phone-number"><?php the_field('call_us_telephone_number','option'); ?></span></a>
            </div>
            <div class="header-search hidden-xs col-xs-12 col-sm-4 pull-right">
                <?php get_template_part('templates/search','form'); ?>
            </div>
        </div>
    </div>
</div>
<header class="banner navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a href="#"  data-toggle="collapse" data-target=".navbar-collapse" class="navbar-title visible-xs pull-left visible-sm"><?php echo wp_trim_words(Titles\title(),3); ; ?></a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <nav class="collapse navbar-collapse">
            <?php
            if (has_nav_menu('primary_navigation')) :
                wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-nav']);
            endif;
            ?>
        </nav>
    </div>
</header>
