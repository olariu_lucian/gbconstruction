<article <?php post_class('service-box col-xs-12 col-sm-6 col-md-4'); ?>>
    <?php
    if (has_post_thumbnail( get_the_ID())) {
        $image = wp_get_attachment_image( get_post_thumbnail_id(get_the_ID()), 'wide-medium',false,array('class' => 'img-responsive thumbnail') );
    } else { $image = ''; }
    ?>
    <a href="<?php the_permalink();?>"><div class="image"><?php echo $image; ?></div>
        <h2 class="text-uppercase service-title"><?php the_title();?></h2></a>
    <div class="serv-desc text-justify"><?php echo wp_trim_words(get_the_excerpt(),30);?></div>
    <a href="<?php the_permalink();?>" title="<?php the_title();?>" class="read-more text-uppercase">Details <i class="fa fa-long-arrow-right"></i></a>
</article>