<section class="hp-reviews white-bg clearfix">
    <div class="container">
        <div class="row clearfix">
            <div class="col-xs-12">
                <a href="<?php the_field('reviews_title_link','option');?>"><h2 class="text-center text-uppercase"><?php the_field('reviews_title_text','option');?></h2></a>
                <div class="text-center"><?php the_field('reviews_title_description','option');?></div>
                <div class="sep"><hr></div>

            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
    <div class="reviews-list clearfix"><?php
    $posts = get_field('home_reviews','option');?>
      <?php  //echo do_shortcode('[WPCR_SHOW POSTID="28" NUM="12" PAGINATE="0" PERPAGE="12" SHOWFORM="0" HIDEREVIEWS="0" HIDERESPONSE="1" SNIPPET="140" MORE="" HIDECUSTOM="0" ] ');?>
      <?php if ($posts) : ?>
              <?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
                    <?php $meta = get_post_meta( $p->ID); ?>
                    <div id="wpcr3_id_<?php echo $p->ID;?>" class="wpcr3_review"  itemprop="review" itemscope itemtype="http://schema.org/Review">
                            <meta itemprop="author" content="<?php echo $meta['wpcr3_review_name'][0]; ?>" />
                          <div class="wpcr3_hide" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                            <meta itemprop="bestRating" content="5" />
                            <meta itemprop="worstRating" content="1" />
                            <meta itemprop="ratingValue" content="<?php echo $meta['wpcr3_review_rating'][0]; ?>" />
                          </div>

                          <div class="wpcr3_review_title wpcr3_caps pull-left"><strong><?php echo $meta['wpcr3_review_title'][0]; ?></strong></div>

                          <div class="wpcr3_review_ratingValue pull-right"><?php // echo $meta['wpcr3_review_rating'][0]; ?>
                            <div class="wpcr3_rating_style1">
                              <div class="wpcr3_rating_style1_base {{:hoverable}}wpcr3_hide{{/hoverable}}">
                                <div class="wpcr3_rating_style1_average" style="width:100%;"></div>
                              </div>
                            </div>
                          </div>
                              <blockquote class="wpcr3_content" itemprop="reviewBody">
                                <p>
                                    "<?php echo $p->post_content; ?>"
                                </p>
                              </blockquote>
                          <div class="wpcr3_review_author pull-right">
                          <span class="wpcr3_caps">
                            <span><?php echo $p->post_title; ?></span>
                          </span>
                          <span class="inlabel">&nbsp;from&nbsp;</span>
                          <span class="wpcr3_item_name">

                          </span>

                          <span class="wpcr3_review_customs">

                              <span class="wpcr3_review_custom">
                                  <span class="wpcr3_review_custom_label wpcr3_hide"></span> <span class="wpcr3_review_custom_value"><?php echo $meta['wpcr3_f1'][0]; ?></span>
                              </span>

                          </span>
                          <div class="wpcr3_clear"></div>

                          </div>

                          <!--  {{:wpcr3_review_admin_response}}
                          <blockquote class="wpcr3_content wpcr3_admin_response">
                          <p>
                              {{wpcr3_review_admin_response}}
                          </p>
                          </blockquote>
                          {{/wpcr3_review_admin_response}} -->
                    </div>

              <?php endforeach; ?>
      <?php endif; ?>
  </div>
    </div>
<!--  <div class="reviews-cta col-xs-12 text-center"><a href="--><?php //the_field('reviews_title_link','option');?><!--" class="btn btn-border text-uppercase">Read more</a></div>-->


</section>
