<section class="hp-accreditations alter-bg">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="text-center text-uppercase"><?php the_field('not_convinced_title','option');?></h2>
                <div class="text-center"><?php the_field('not_convinced_description','option');?></div>
                <div class="sep"><hr></div>
            </div>

        </div>
    </div>
    <div class="container-fluid">
    <ul class="acc-logos col-xs-12 text-center">
        <?php dynamic_sidebar('sidebar-homesocial'); ?>
    </ul>
    </div>
</section>
