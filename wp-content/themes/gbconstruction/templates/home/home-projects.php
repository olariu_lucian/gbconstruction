<?php
  $num_posts = get_field('recent_projects_number_of_posts','option');
?>
<section class="hp-projects alter-bg">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="text-center text-uppercase"><?php the_field('recent_project_title_text','option');?></h2>
                <div class="text-center"><?php the_field('recent_projects_description','option');?></div>
                <div class="sep"><hr></div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 clearfix">
        <div class="home-projects owl-carousel owl-theme">
            <?php
            $projects_args = array(
                'post_type' => 'projects',
                'posts_per_page' => $num_posts,
                'meta_key'=>'_thumbnail_id',
                'orderby' => 'date',
                'order' => 'ASC',
                'post_status' => 'publish'
            );
            $hp_projects = get_posts($projects_args);
            foreach($hp_projects as $project) {
                $attachment_id = get_post_thumbnail_id( $project->ID );
                $image_link = wp_get_attachment_image_src($attachment_id,'large',false);
                $image = wp_get_attachment_image_src(get_post_thumbnail_id( $project->ID ), 'wide-medium', false);
                if (has_post_thumbnail( $project->ID )) {
                    $proj_image = wp_get_attachment_image( get_post_thumbnail_id( $project->ID ), 'wide-medium', false, array('class' => 'img-responsive thumbnail') );
                } else { $proj_image = ''; }
                ?>
                <div class="project-box">
                    <a href="<?php //echo get_permalink($project->ID);
                    echo $image_link[0]; ?>">
                        <?php //echo $proj_image; ?>
                        <img src="http://placehold.it/400x190" class="owl-lazy thumbnail" data-src="<?php echo $image[0];?>" alt="project-image">
                    </a>
                </div>
            <?php } ?>
        </div>
        </div>
    </div>


</section>
