<div class="hp-share white-bg">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <?php do_action('social_share_icons'); ?>
            </div>
        </div>
    </div>
</div>