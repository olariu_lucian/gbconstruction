<section class="hp-services white-bg">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="text-center text-uppercase"><?php the_field('services_title','option');?></h2>
                <div class="text-center"><?php the_field('services_description','option');?> </div>
                <div class="sep"><hr></div>
            </div>
            <?php
            $serv_args = array(
                'post_type' => 'services',
                'posts_per_page' => 6,
                'orderby' => 'menu_order',
                'order' => 'ASC',
                'post_status' => 'publish'
            );
            $services_query = new WP_Query( $serv_args );
            if ( $services_query->have_posts() ) {
                while ( $services_query->have_posts() ) : $services_query->the_post();
                    if (has_post_thumbnail()) {
                        $image = wp_get_attachment_image( get_post_thumbnail_id(), 'wide-medium',false,array('class' => 'img-responsive thumbnail') );
                    } else { $image = ''; } ?>
                   <article class="service-box col-xs-12 col-sm-6 col-md-4">
                        <a href="<?php the_permalink();?>"><div class="image"><?php echo $image; ?></div>
                            <h2 class="text-uppercase service-title"><?php the_title();?></h2></a>
                        <div class="serv-desc text-justify"><?php echo wp_trim_words(get_the_excerpt(),30);?></div>
                        <a href="<?php the_permalink();?>" title="<?php the_title();?>" class="read-more text-uppercase">Details <i class="fa fa-long-arrow-right"></i></a>
                    </article>
                <?php endwhile;
            } else {
                // no posts found
            }
            /* Restore original Post Data */
            wp_reset_postdata();




//            $hp_services = get_posts($serv_args);
//            foreach($hp_services as $service) {
//                if (has_post_thumbnail( $service->ID )) {
//                    $image = wp_get_attachment_image( get_post_thumbnail_id( $service->ID ), 'wide-medium',false,array('class' => 'img-responsive thumbnail') );
//                } else { $image = ''; } ?>
<!--                <div class="service-box col-xs-12 col-sm-4">-->
<!--                    <a href="--><?php //echo get_permalink($service->ID);?><!--"><div class="image">--><?php //echo $image; ?><!--</div>-->
<!--                        <h3 class="text-uppercase">--><?php //echo $service->post_title;?><!--</h3></a>-->
<!--                    <div class="serv-desc">--><?php //echo wp_trim_words($service->post_excerpt,20);?><!--</div>-->
<!--                    <a href="--><?php //echo get_permalink($service->ID);?><!--" title="--><?php //echo $service->post_title;?><!--" class="read-more text-uppercase">Details <i class="fa fa-long-arrow-right"></i></a>-->
<!--                </div>-->
<!--            --><?php //} ?>
        </div>
        <div class="row">
            <div class="col-xs-12 text-center"><a href="<?php echo get_post_type_archive_link('services');?>" class="btn btn-border text-uppercase"><?php the_field('services_call_to_action_button_text','option');?></a></div>
        </div>
    </div>
</section>
