<?php
$intro_image = get_field('intro_image','option');

?>
<section class="hp-intro" style="background: url('<?php echo $intro_image['sizes']['large']?>') no-repeat 0 0 transparent; background-attachment:scroll; background-size:cover">
    <div class="container">
        <div class="row clear-fix">
            <div class="intro-text col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 text-center text-uppercase">
              <h1><?php  the_field('intro_image_text', 'option'); ?></h1>
              <p class="intro-image-description"><?php the_field('intro_image_description','option'); ?></p>
            </div>
        </div>
    </div>
</section>
