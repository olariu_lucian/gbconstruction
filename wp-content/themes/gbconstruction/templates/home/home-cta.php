<section class="cta-contact alter-bg">
    <div class="container">
        <div class="row">
            <div class="cta-text col-xs-12 col-sm-9 col-md-10">
                <?php the_field('call_to_action_area_text','option');?>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-2 text-center">
                <a href=<?php echo '"', the_field('call_to_action_button_link','option'), '"'; ?> class="btn btn-default text-uppercase"><?php the_field('call_to_action_button_label','option')?></a></div>
        </div>
    </div>
</section>
