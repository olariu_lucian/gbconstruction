<?php
$footer_bg_image = get_field('footer_background_image','option');
?>

<div class="pre-footer clearfix" style="background-image: url('<?php echo $footer_bg_image['sizes']['large']?>')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row clearfix">
    <?php dynamic_sidebar('sidebar-footer'); ?>
        </div>
    </div>
</div>
<footer class="content-info">
  <div class="container">
      <div class="row">
    <div class="col-xs-12 col-sm-6 copyright"> <?php the_field('footer_rights','option');?></div>
    <div class="col-xs-12 col-sm-6 credits"><?php the_field('footer_credits','option');?></div>
      </div>
  </div>
</footer>
