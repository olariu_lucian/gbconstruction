<?php
$categories = get_the_category();
$separator = ', ';
$output = '';
?>
<time class="updated" datetime="<?php echo get_post_time('c', true); ?>"><?php echo get_the_date(); ?></time>
<div class="byline author vcard">
    <?php //echo __(' - written by ', 'gbconstruction'); ?>
<!--    <a href="--><?php //echo get_author_posts_url(get_the_author_meta('ID')); ?><!--" rel="author" class="fn">--><?php //echo get_the_author(); ?><!--</a>-->
    <span>Category: <?php if ( ! empty( $categories ) ) {
            foreach( $categories as $category ) {
                $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
            }
            echo trim( $output, $separator );
        } ?></span>
</div>