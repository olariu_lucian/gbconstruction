<?php
global $postnum;
$class_text = 'col-xs-12';

if ($postnum % 2 && has_post_thumbnail()) { //even
  $class_img = 'col-xs-12 col-sm-5';
  $class_text = 'col-xs-12 col-sm-7';
} else { // odd
  $class_img = 'col-xs-12 col-sm-5';
  $class_text = 'col-xs-12 col-sm-7';
}

?>

<article <?php post_class('clearfix'); ?>>
<div class="col-md-12 no-padding">
  <?php if(has_post_thumbnail()) { ?>
  <div class="<?php echo $class_img; ?>">
      <?php the_post_thumbnail('wide-medium', array('class' => 'img-responsive thumbnail'));?>
  </div>
 <?php } ?>

  <div class="<?php echo $class_text; ?>">
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php get_template_part('templates/entry-meta'); ?>
    <div class="excerpt pull-left"><?php echo wp_trim_words(get_the_excerpt(),20, '...'); ?></div>
    <p class="pull-right"><a href="<?php the_permalink(); ?>" class="read-more text-uppercase">Read more <i class="fa fa-long-arrow-right"></i></a></p>
  </div>
</div>
</article>
<div class="sep clearfix"><hr></div>
