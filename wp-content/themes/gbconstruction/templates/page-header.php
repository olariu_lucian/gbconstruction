<?php
use ImpactPro\Titles;
$header_image = get_field('title_image','option');
?>

<div class="page-header" style="background-image:url('<?php echo $header_image['sizes']['large'];?>')">
    <div class="overlay"></div>
  <div class="container">
    <div class="row clearfix">
      <div class="col-xs-12 text-center">
        <h1 class="text-uppercase"><?php echo Titles\title(); ?></h1>
        <?php if ( function_exists('yoast_breadcrumb') )
        {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
      </div>
    </div>
  </div>
</div>
