<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class(); ?>>
        <div class="entry-content">
            <?php the_content(); ?>
        </div>
        <footer>
            <div class="text-center white-bg">
           <?php do_action('social_share_icons'); ?>
            </div>
        </footer>
        <?php // comments_template('/templates/comments.php'); ?>
    </article>
<?php endwhile; ?>
