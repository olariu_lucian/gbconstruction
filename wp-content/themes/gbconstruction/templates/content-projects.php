<?php
$posts_term = wp_get_object_terms(get_the_ID(),'projects_types');
$content = get_the_content();
$attachment_id = get_post_thumbnail_id( $post->ID );
$image_link = wp_get_attachment_image_src($attachment_id,'large',false);
?>

<div class="col-xs-12 col-sm-4 col-md-3 text-center grid-item element-item <?php foreach ($posts_term as $post_term) {echo ' '.$post_term->slug; } ?>">
    <header>
      <a href="<?php
//      if(!empty($content)) {
//          echo get_the_permalink(get_the_ID());
//      } else {
          echo $image_link[0];
      //}?>" title="<?php the_title(); ?>">
            <?php
            if ( has_post_thumbnail() ) {
                 the_post_thumbnail('wide-medium', array('class' => 'img-responsive portofoliu-img thumbnail') );
            }
            ?>

<!--            <h2 class="entry-title">--><?php //the_title(); ?><!--</h2>-->
      </a>
      <?php //get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-summary">
      <?php //the_excerpt(); ?>
    </div>
</div>
