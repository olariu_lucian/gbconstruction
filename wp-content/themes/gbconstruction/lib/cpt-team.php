<?php
if ( ! function_exists('team_members_post') ) {

// Register Custom Post Type
    function team_members_post() {

        $labels = array(
            'name'                  => _x( 'Team', 'Post Type General Name', 'sage' ),
            'singular_name'         => _x( 'Team', 'Post Type Singular Name', 'sage' ),
            'menu_name'             => __( 'Team Members', 'sage' ),
            'name_admin_bar'        => __( 'Team', 'sage' ),
            'archives'              => __( 'Item Archives', 'sage' ),
            'parent_item_colon'     => __( 'Parent Item:', 'sage' ),
            'all_items'             => __( 'All Team Members', 'sage' ),
            'add_new_item'          => __( 'Add New Member', 'sage' ),
            'add_new'               => __( 'Add New', 'sage' ),
            'new_item'              => __( 'New Item', 'sage' ),
            'edit_item'             => __( 'Edit Team Member', 'sage' ),
            'update_item'           => __( 'Update Team Member', 'sage' ),
            'view_item'             => __( 'View Team Member', 'sage' ),
            'search_items'          => __( 'Search Team Member', 'sage' ),
            'not_found'             => __( 'Team member not found', 'sage' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'sage' ),
            'featured_image'        => __( 'Featured Image', 'sage' ),
            'set_featured_image'    => __( 'Set featured image', 'sage' ),
            'remove_featured_image' => __( 'Remove featured image', 'sage' ),
            'use_featured_image'    => __( 'Use as featured image', 'sage' ),
            'insert_into_item'      => __( 'Insert into item', 'sage' ),
            'uploaded_to_this_item' => __( 'Uploaded to this item', 'sage' ),
            'items_list'            => __( 'Items list', 'sage' ),
            'items_list_navigation' => __( 'Items list navigation', 'sage' ),
            'filter_items_list'     => __( 'Filter items list', 'sage' ),
        );
        $args = array(
            'label'                 => __( 'Team', 'sage' ),
            'description'           => __( 'Our team members', 'sage' ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'page-attributes', ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 20,
            'menu_icon'             => 'dashicons-groups',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => true,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
        );
        register_post_type( 'team', $args );

    }
    add_action( 'init', 'team_members_post', 0 );

}