<?php
// Register Custom Post Type
function projects_post_type() {

    $labels = array(
        'name'                  => _x( 'Projects', 'Post Type General Name', 'sage' ),
        'singular_name'         => _x( 'Project', 'Post Type Singular Name', 'sage' ),
        'menu_name'             => __( 'Projects', 'sage' ),
        'name_admin_bar'        => __( 'Projects', 'sage' ),
        'archives'              => __( 'Project Archives', 'sage' ),
        'parent_item_colon'     => __( 'Parent Project:', 'sage' ),
        'all_items'             => __( 'All Projects', 'sage' ),
        'add_new_item'          => __( 'Add New Project', 'sage' ),
        'add_new'               => __( 'Add New Project', 'sage' ),
        'new_item'              => __( 'New Project', 'sage' ),
        'edit_item'             => __( 'Edit Project', 'sage' ),
        'update_item'           => __( 'Update Project', 'sage' ),
        'view_item'             => __( 'View Project', 'sage' ),
        'search_items'          => __( 'Search Project', 'sage' ),
        'not_found'             => __( 'Not found', 'sage' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'sage' ),
        'featured_image'        => __( 'Project Image', 'sage' ),
        'set_featured_image'    => __( 'Set project image', 'sage' ),
        'remove_featured_image' => __( 'Remove project image', 'sage' ),
        'use_featured_image'    => __( 'Use as project image', 'sage' ),
        'insert_into_item'      => __( 'Insert into project', 'sage' ),
        'uploaded_to_this_item' => __( 'Uploaded to this project', 'sage' ),
        'items_list'            => __( 'Projects list', 'sage' ),
        'items_list_navigation' => __( 'Projects list navigation', 'sage' ),
        'filter_items_list'     => __( 'Filter projects list', 'sage' ),
    );
    $rewrite = array(
        'slug'                  => 'latest-projects',
        'with_front'            => true,
        'pages'                 => false,
        'feeds'                 => false,
    );
    $args = array(
        'label'                 => __( 'Project', 'sage' ),
        'description'           => __( 'Recent Projects', 'sage' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'page-attributes', ),
        'taxonomies'            => array( 'projects_types' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'dashicons-images-alt',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type( 'projects', $args );

}
add_action( 'init', 'projects_post_type', 0 );

// Register Custom Taxonomy
function projects_types_taxonomy() {

    $labels = array(
        'name'                       => _x( 'Project Types', 'Taxonomy General Name', 'sage' ),
        'singular_name'              => _x( 'Project Type', 'Taxonomy Singular Name', 'sage' ),
        'menu_name'                  => __( 'Project Types', 'sage' ),
        'all_items'                  => __( 'All Project Types', 'sage' ),
        'parent_item'                => __( 'Parent Project Type', 'sage' ),
        'parent_item_colon'          => __( 'Parent Project Type:', 'sage' ),
        'new_item_name'              => __( 'New Project Type', 'sage' ),
        'add_new_item'               => __( 'Add New Project Type', 'sage' ),
        'edit_item'                  => __( 'Edit Project Type', 'sage' ),
        'update_item'                => __( 'Update Project Type', 'sage' ),
        'view_item'                  => __( 'View Project Type', 'sage' ),
        'separate_items_with_commas' => __( 'Separate project types with commas', 'sage' ),
        'add_or_remove_items'        => __( 'Add or remove project types', 'sage' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'sage' ),
        'popular_items'              => __( 'Popular Project Types', 'sage' ),
        'search_items'               => __( 'Search Project Types', 'sage' ),
        'not_found'                  => __( 'Not Found', 'sage' ),
        'no_terms'                   => __( 'No project types', 'sage' ),
        'items_list'                 => __( 'Project Types list', 'sage' ),
        'items_list_navigation'      => __( 'Project types list navigation', 'sage' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
        'rewrite'                    => false,
    );
    register_taxonomy( 'projects_types', array( 'projects' ), $args );

}
add_action( 'init', 'projects_types_taxonomy', 0 );
