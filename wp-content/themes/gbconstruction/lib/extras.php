<?php

namespace ImpactPro\Extras;

use ImpactPro\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'gbconstruction') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Base for CPT
 */
add_filter('sage/wrap_base', __NAMESPACE__ . '\\sage_wrap_base_cpts'); // Add our function to the sage_wrap_base filter

function sage_wrap_base_cpts($templates) {
    $cpt = get_post_type(); // Get the current post type
    if ($cpt) {
        array_unshift($templates, 'base-' . $cpt . '.php'); // Shift the template to the front of the array
    }
    return $templates; // Return our modified array with base-$cpt.php at the front of the queue
}

/************* ADD Featured Image column and thumbnails *****************/
// Add the posts and pages columns filter. They can both use the same function.
add_filter('manage_posts_columns',  __NAMESPACE__ . '\\sage_add_post_thumbnail_column', 2);
add_filter('manage_pages_columns',  __NAMESPACE__ . '\\sage_add_post_thumbnail_column', 2);

// Add the column
function sage_add_post_thumbnail_column($cols){
    $cols['sage_post_thumb'] = __('Featured Image','sage');
    return $cols;
}

// Hook into the posts an pages column managing. Sharing function callback again.
add_action('manage_posts_custom_column', __NAMESPACE__ . '\\sage_display_post_thumbnail_column', 2, 2);
add_action('manage_pages_custom_column', __NAMESPACE__ . '\\sage_display_post_thumbnail_column', 2, 2);

// Grab featured-thumbnail size post thumbnail and display it.
function sage_display_post_thumbnail_column($col, $id){
    switch($col){
        case 'sage_post_thumb':
            if( function_exists('the_post_thumbnail') )
                echo the_post_thumbnail(array(120,70) );
            else
                echo 'Not supported in theme';
            break;
    }
}

/************* login logo url change *****************/
function change_wp_login_url() {
    return home_url();
}
add_filter('login_headerurl', __NAMESPACE__ . '\\change_wp_login_url');

/************* login logo title change *****************/

function change_wp_login_title() {
    return get_option('blogname');
}
add_filter('login_headertitle', __NAMESPACE__ . '\\change_wp_login_title');

function remove_footer_admin () {
    echo 'Created by <a href="http://www.impactpro.ro" target="_blank">Impact Production.</a>';
}
add_filter('admin_footer_text', __NAMESPACE__ . '\\remove_footer_admin');

// Hide wp icon and links from the admin bar
function annointed_admin_bar_remove() {
    global $wp_admin_bar;
    /* Remove their stuff */
    $wp_admin_bar->remove_menu('wp-logo');
}
add_action('wp_before_admin_bar_render', __NAMESPACE__ . '\\annointed_admin_bar_remove', 0);

// login logo
function my_login_logo() {
    $logo_image = get_field('logo_image','option');
    //$upload_dir = wp_upload_dir();
    //$admin_logo = $admin_logo_field['large'];
    ?>
    <style type="text/css">
        body.login div#login h1 a {background-image: url( <?php echo $logo_image['url'];?>); background-size: 100%; padding-bottom: 0; width:176px; height:134px; }
        body.login {background-color: #F8F6EE}
        #login {padding-top:30px;}

    </style>
<?php }
add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\my_login_logo' );
/**
 * Add extra image sizes in WP-admin
 */
function my_insert_custom_image_sizes( $sizes ) {
    global $_wp_additional_image_sizes;
    if ( empty($_wp_additional_image_sizes) )
        return $sizes;

    foreach ( $_wp_additional_image_sizes as $id => $data ) {
        if ( !isset($sizes[$id]) )
            $sizes[$id] = ucfirst( str_replace( '-', ' ', $id ) );
    }

    return $sizes;
}
add_filter( 'image_size_names_choose', __NAMESPACE__ . '\\my_insert_custom_image_sizes' );
/**
 * Social Share Icons
 */
add_action('social_share_icons', __NAMESPACE__ . '\\gb_social_sharing_buttons', 10);
function gb_social_sharing_buttons() {
    // Show this on post and page only. Add filter is_home() for home page
    global $post;
    // Get current page URL
    $shortURL = get_permalink();

    // Get current page title
    $theTitle = get_the_title();
    $shortTitle = str_replace(' ','%20',$theTitle);

    // Construct sharing URL without using any script
    $twitterURL = 'https://twitter.com/intent/tweet?text='.$shortTitle.'&amp;url='.$shortURL.'&amp;via=GBConstructions';
    $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$shortURL;
    $googleURL = 'https://plus.google.com/share?url='.$shortURL;
    //$bufferURL = 'https://bufferapp.com/add?url='.$shortURL.'&amp;text='.$shortTitle;
    $pinterestURL = 'http://pinterest.com/pin/create/button/?url='.$shortURL.'&amp;description='.$shortTitle;
    $stumbleURL = 'http://www.stumbleupon.com/submit?url='.$shortURL.'&amp;title='.$shortTitle;

    // Add sharing button at the end of page/page content
    $content = '';
    $content .= 'Share on: ';
    $content .= '<div class="social-share-icons text-center hidden-print">';
    $content .= '<a class="gb-share-link btn btn-sm woo-twitter" href="'. $twitterURL .'" target="_blank"><i class="fa fa-twitter"></i> <span class="hidden-xs">Twitter</span></a>';
    $content .= '<a class="gb-share-link btn btn-sm woo-facebook" href="'.$facebookURL.'" target="_blank"><i class="fa fa-facebook"></i> <span class="hidden-xs">Facebook</span></a>';
    $content .= '<a class="gb-share-link btn btn-sm woo-googleplus" href="'.$googleURL.'" target="_blank"><i class="fa fa-google-plus"></i> <span class="hidden-xs">Google+</span></a>';
    $content .= '<a class="gb-share-link btn btn-sm woo-pinterest" href="'.$pinterestURL.'" target="_blank"><i class="fa fa-pinterest"></i> <span class="hidden-xs">Pinterest</span></a>';
    $content .= '<a class="gb-share-link btn btn-sm woo-stumble" href="'.$stumbleURL.'" target="_blank"><i class="fa fa-stumbleupon"></i> <span class="hidden-xs">StumbleUpon</span></a>';
    //$content .= '<a class="woo-share-link woo-buffer" href="'.$bufferURL.'" target="_blank">Buffer</a>';
    $content .= '</div>';
    echo $content;
};
/**
 * Remove WP generator
 */
remove_action('wp_head', 'wp_generator');

/**
 * Remove WP Emoji
 */
function disable_wp_emojicons() {
    // all actions related to emojis
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

    // filter to remove TinyMCE emojis
    add_filter( 'tiny_mce_plugins', __NAMESPACE__ . '\\disable_emojicons_tinymce' );
}
add_action( 'init', __NAMESPACE__ . '\\disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}
/**
 * Projects posts per page
 */
add_action( 'pre_get_posts', __NAMESPACE__ . '\\pre_get_post_projects' );
function pre_get_post_projects( $query ) {
    if( is_post_type_archive( array('projects','services') ) && !is_admin() && $query->is_main_query() ) {
        $query->set( 'posts_per_page', -1 );
        $query->set( 'orderby', 'menu_order' );
        $query->set( 'order', 'ASC' );
    }
}

add_filter ('wpseo_breadcrumb_output', __NAMESPACE__ . '\\mc_microdata_breadcrumb');
function mc_microdata_breadcrumb ($link_output) {
    $link_output = preg_replace(array('#<span xmlns:v="http://rdf.data-vocabulary.org/\#">#','#<span typeof="v:Breadcrumb"><a href="(.*?)" .*?'.'>(.*?)</a></span>#','#<span typeof="v:Breadcrumb">(.*?)</span>#','# property=".*?"#','#</span>$#'), array('','<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="$1" itemprop="url"><span itemprop="title">$2</span></a></span>','<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">$1</span></span>','',''), $link_output);

    return $link_output;
}

// add searchform to nav menu
add_filter('wp_nav_menu_items', __NAMESPACE__ . '\\add_search_box_to_menu', 10, 2);
function add_search_box_to_menu( $items, $args ) {

 if( $args->theme_location == 'primary_navigation' ) :
  $items .= '<li class="dropdown visible-xs menu-item menu-icon menu-search"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-search"></i></a>
            <ul class="dropdown-menu" style="padding:8px;">
                <form role="search" method="get" class="search-form form-inline" action="'.esc_url(home_url('/')).'">
  <label class="sr-only">'. __('Search for:', 'sage').'</label>
  <div class="input-group">
    <input type="search" value="'.get_search_query().'" name="s" class="navbar-search search-field form-control input-sm" placeholder="'. __('Search', 'sage').'" required>
    <span class="input-group-btn">
      <button type="submit" class="search-submit btn"><i class="fa fa-search"></i></button>
    </span>
  </div>
</form>
              </ul></li>';
 endif;
 return $items;

}
