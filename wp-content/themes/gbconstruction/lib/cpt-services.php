<?php
// Register Custom Post Type
function services_post_type() {

    $labels = array(
        'name'                  => _x( 'Services', 'Post Type General Name', 'sage' ),
        'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'sage' ),
        'menu_name'             => __( 'Services', 'sage' ),
        'name_admin_bar'        => __( 'Services', 'sage' ),
        'archives'              => __( 'Service Archives', 'sage' ),
        'parent_item_colon'     => __( 'Parent Service:', 'sage' ),
        'all_items'             => __( 'All Services', 'sage' ),
        'add_new_item'          => __( 'Add New Service', 'sage' ),
        'add_new'               => __( 'Add New Service', 'sage' ),
        'new_item'              => __( 'New Service', 'sage' ),
        'edit_item'             => __( 'Edit Service', 'sage' ),
        'update_item'           => __( 'Update Service', 'sage' ),
        'view_item'             => __( 'View Service', 'sage' ),
        'search_items'          => __( 'Search Service', 'sage' ),
        'not_found'             => __( 'Not found', 'sage' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'sage' ),
        'featured_image'        => __( 'Service Image', 'sage' ),
        'set_featured_image'    => __( 'Set service image', 'sage' ),
        'remove_featured_image' => __( 'Remove service image', 'sage' ),
        'use_featured_image'    => __( 'Use as service image', 'sage' ),
        'insert_into_item'      => __( 'Insert into service', 'sage' ),
        'uploaded_to_this_item' => __( 'Uploaded to this service', 'sage' ),
        'items_list'            => __( 'Services list', 'sage' ),
        'items_list_navigation' => __( 'Services list navigation', 'sage' ),
        'filter_items_list'     => __( 'Filter services list', 'sage' ),
    );
    $args = array(
        'label'                 => __( 'Service', 'sage' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'page-attributes', ),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'dashicons-editor-ul',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'services', $args );

}
add_action( 'init', 'services_post_type', 0 );