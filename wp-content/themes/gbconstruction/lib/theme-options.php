<?php
/*
*  Change the Options Page capability to 'manage_options'
*/

if( function_exists('acf_set_options_page_capability') ) {
    acf_set_options_page_capability( 'manage_options' );
}

// 3. Hide ACF field group menu item
//add_filter('acf/settings/show_admin', '__return_false');

// 4. Include ACF
include_once( get_stylesheet_directory() . '/lib/acf/sidebar-selector/acf-sidebar_selector.php' );
include_once( get_stylesheet_directory() . '/lib/acf/image-select/acf-image-select.php' );
include_once( get_stylesheet_directory() . '/lib/acf/recaptcha-field/acf-recaptcha.php' );
include_once( get_stylesheet_directory() . '/lib/acf/field-font-awesome/acf-font-awesome.php' );

// 5. Generate Theme Options Pages
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Options',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));
}