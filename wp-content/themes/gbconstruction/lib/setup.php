<?php

namespace ImpactPro\Setup;

use ImpactPro\Assets;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('gbconstruction', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'gbconstruction')
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
    add_theme_support('post-thumbnails');
    add_image_size('wide-thumb',230,108,true);
    add_image_size('wide-medium',400,190,true);

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Primary', 'gbconstruction'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
  register_sidebar([
      'name'          => __('Reviews', 'gbconstruction'),
      'id'            => 'sidebar-reviews',
      'before_widget' => '<section class="widget %1$s %2$s">',
      'after_widget'  => '</section>',
      'before_title'  => '<h4 class="widget-title hidden">',
      'after_title'   => '</h4>'
  ]);
  register_sidebar([
      'name'          => __('Home Logos', 'gbconstruction'),
      'id'            => 'sidebar-homesocial',
      //'before_widget' => '<div class="widget col-xs-6 col-sm-4 col-md-3 col-lg-2 %1$s %2$s">',
      'before_widget' => '<li class="widget %1$s %2$s">',
      'after_widget'  => '</li>',
      'before_title'  => '<h4 class="widget-title hidden">',
      'after_title'   => '</h4>'
  ]);

  register_sidebar([
    'name'          => __('Footer', 'gbconstruction'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget col-xs-12 col-sm-4 %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h4 class="text-uppercase">',
    'after_title'   => '</h4>'
  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;
  global $post;
  $display_sidebar = get_field('display_sidebar');
  $hide_sidebar = '';
  if ($display_sidebar == '' && !is_home() && !is_category() && !is_singular('post') && !is_search())  {
    $hide_sidebar = ['is_page',array($post->ID)];
  } elseif($display_sidebar == 1 ) {
    $hide_sidebar = '';
  }
  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
      $hide_sidebar,
    is_front_page(),
      is_post_type_archive(array('team','projects','services')),
    is_page_template('template-custom.php'),
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
    wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:400,400italic,500,700,700italic,300,300italic,500,500italic%7CRaleway:700,400',false,null);
    wp_enqueue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css',false,null);
  wp_enqueue_style('gbconstruction', Assets\asset_path('styles/main.css'), false, null);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_enqueue_script('impactpro/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);

}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);
