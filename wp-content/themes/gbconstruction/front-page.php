<?php get_template_part('templates/home/home','intro');?>
<?php get_template_part('templates/home/home','cta');?>
<?php get_template_part('templates/home/home','services');?>
<?php get_template_part('templates/home/home','projects');?>
<?php get_template_part('templates/home/home','reviews');?>
<?php get_template_part('templates/home/home','not-convinced');?>
<?php get_template_part('templates/home/home','share');?>
<?php while (have_posts()) : the_post(); ?>
    <?php //get_template_part('templates/page', 'header'); ?>
    <?php //get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
