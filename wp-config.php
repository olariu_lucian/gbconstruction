<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'gbconstruction');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3xPP}=R]s!5?Z>_C,0+S}xfx9:-Bd|t-[5Xm#V5-q1YLF[cM<,CKvKIw7]f# jp!');
define('SECURE_AUTH_KEY',  '-3$S{oUA)Z6s$DX~B(-`gEu{L@V>0gI_QJJ.?uF)#fs`{+j5+q79iOwW>M9O1I}y');
define('LOGGED_IN_KEY',    'fe2S0`mus_I]em=IyT>OWWNoG2;b2@zNoern-<Zj(6JU8E*Vl7SZ?oI^t&>+BZ.r');
define('NONCE_KEY',        '(Gd]y3DZMIA~Ga*>ba=,*,n+VKR1qt2f+U=pA1YPZ<c9xX{:qBvdY-+1U5]x7-`m');
define('AUTH_SALT',        '_->!:}+Et8lS[+scmH+vGyc`!cT^lgkYj{O:M]pNdqn0;Br8i@1V&:R8qk(=lx.y');
define('SECURE_AUTH_SALT', 'E`X`1><M@(I&FT2EC6Iy D_#c;l?F!o|r:k_&jy&M@S8$%R%(O9yhN)T.1m$_$S!');
define('LOGGED_IN_SALT',   'KmC,6:w!u8Y Nj7| &%pp>^|c>z&x-*sv1pspX!mO*XBE*?1|ad]c]u>XrQ#36L{');
define('NONCE_SALT',       '<6ASqe`<]{._`UV6Gu;SLvL.;a7MK]7$xz%36A|w2LS@Ap&cGQ?q)72(G[Vh9TqW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'gbc16_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
